var logger = loggerFactory( 'RactiveModal' );
RactiveModal = Ractive.extend( {
    append: true,
    template: function () {return Ract.getTemplate( 'RactiveModal' )},

    grabElements: function () {
//        logger.log('RactiveModal : grabElements : ', this);
        this.background = this.find( '.ractiveModal_background' );
        this.outer = this.find( '.ractiveModal_outer' );
        this.modal = this.find( '.ractiveModal_inner' );
    },

    oninit: function () {
        var self = this, resizeHandler;

        // if the user taps on the background, close the modal
        this.on( 'close', function ( el ) {
            if ( !this.modal.contains( el.original.target ) ) {
                this.teardown();
            }
        } );

        // when the window resizes, keep the modal horizontally and vertically centered
        window.addEventListener( 'resize', resizeHandler = function () {
            self.center();
        }, false );

        // clean up after ourselves later
        this.on( 'teardown', function () {
            window.removeEventListener( 'resize', resizeHandler );
        }, false );

    },

    onrender: function () {
        var self = this;
        // store references to the background, and to the modal itself
        // we'll assume we're in a modern browser and use querySelector
        self.grabElements();
        // manually call this.center() the first time
//        self.logger.log(this);
        this.center();
    },
    center: function () {
        try {
            var outerHeight, modalHeight, verticalSpace;

            // horizontal centring is taken care of by CSS, but we need to
            // vertically centre
            backgroundHeight = this.background.clientHeight;
            modalHeight = this.modal.clientHeight;

            verticalSpace = ( backgroundHeight - modalHeight ) / 2;

            this.modal.style.top = (verticalSpace - 10) + 'px';
        } catch ( e ) {
            logger.error( "center | error was caught : ", e );
        }
    }
} );