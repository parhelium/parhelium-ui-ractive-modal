Package.describe({
  name:'parhelium:ui-ractive-modal',
  summary: "UI - modal ",
  version: "1.0.0"
});

Package.onUse(function(api) {
    api.versionsFrom('METEOR@0.9.0');

    api.use([
        'parhelium:logger',
        'parhelium:ractive@0.6.0',
        'parhelium:templating-ractive'
    ], ['client']);

    api.imply('parhelium:ractive@0.6.0')
    api.imply('parhelium:templating-ractive')

    api.addFiles(
        [
            'modal/RactiveModal.js',
            'modal/RactiveModal.ract',
            'modal/RactiveModal.css'
        ],
        'client'
    );
    api.export("RactiveModal",'client');
});

